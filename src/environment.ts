export const environment = {
    apiUrl: 'https://api.kapture.la/api/v1',
    urlValidateAccount: 'https://online.kapture.la/account-validation',
};
